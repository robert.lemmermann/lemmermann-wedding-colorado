import React from 'react'
import ReactDOM from 'react-dom'
import './app/static/styles/entry.css'
import App from './app/components/App'
import createStore from './app/store/createStore'

const store = createStore(window.__STATE__)

ReactDOM.render(
  <App store={store}/>,
  document.getElementById('root')
)
