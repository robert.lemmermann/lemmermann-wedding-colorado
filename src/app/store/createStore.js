import { compose, createStore as reduxCreateStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../store/reducers/root-reducer'

const createStore = () => reduxCreateStore(
  rootReducer,
  compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f) => f
  )
)

export default createStore
