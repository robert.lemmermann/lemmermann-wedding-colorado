import {
  LODGING_CLICKED,
  CEREMONY_CLICKED,
  ACTIVITIES_CLICKED,
  HOME_CLICKED,
  TIMELINE_CLICKED,
  DINNER_CLICKED
} from '../static/types'

export const lodgingClicked = () => dispatch => {
  dispatch({ type: LODGING_CLICKED })
}

export const ceremonyClicked = () => dispatch => {
  dispatch({ type: CEREMONY_CLICKED })
}

export const activitiesClicked = () => dispatch => {
  dispatch({ type: ACTIVITIES_CLICKED })
}

export const homeClicked = () => dispatch => {
  dispatch({ type: HOME_CLICKED })
}

export const timelineClicked = () => dispatch => {
  dispatch({ type: TIMELINE_CLICKED })
}

export const dinnerClicked = () => dispatch => {
  dispatch({ type: DINNER_CLICKED })
}
