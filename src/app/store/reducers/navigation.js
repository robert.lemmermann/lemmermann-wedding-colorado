import { 
  CEREMONY_CLICKED,
  LODGING_CLICKED,
  ACTIVITIES_CLICKED,
  HOME_CLICKED,
  TIMELINE_CLICKED,
  DINNER_CLICKED
} from '../../static/types'

export const initialState = {
  currentTab: 'home'
}

const navigationReducer = (state = initialState, { type, payload = {} }) => {
  switch (type) {
    case CEREMONY_CLICKED:
      return {
        ...state,
        currentTab: 'ceremony'
      }
    case LODGING_CLICKED:
      return {
        ...state,
        currentTab: 'lodging'
      }
    case ACTIVITIES_CLICKED:
      return {
        ...state,
        currentTab: 'activities'
      }
    case HOME_CLICKED:
      return {
        ...state,
        currentTab: 'home'
      }
    case TIMELINE_CLICKED:
      return {
        ...state,
        currentTab: 'timeline'
      }
    case DINNER_CLICKED:
      return {
        ...state,
        currentTab: 'dinner'
      }
    default:
      return state
  }
}

export default navigationReducer
