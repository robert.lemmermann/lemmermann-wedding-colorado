import React from 'react'
import { Grid } from 'semantic-ui-react'
import { BorderlessPaddedSegment, JustifiedRow, MainText, PaddedHeader, SubText } from './shared-components'

const { Column } = Grid

const Timeline = () => (
  <>
    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow verticalAlign='middle'>
          <Column verticalAlign='middle'>
            <PaddedHeader as='h3'>Timeline of Events</PaddedHeader>
            <MainText>1:45 PM | Ceremony </MainText>
            <SubText>Note: it takes 10 minutes to walk from the parking lot to the ceremony site.</SubText>
            <MainText>2:30 PM | Guest Photos </MainText>
            <MainText>3:00 PM to 4:30 PM | Feel free to enjoy the beauty of Rifle Falls and head to Rifle Gap whenever you are ready!</MainText>
            <MainText>4:45 PM | Bride and Groom arrive at Rifle Gap</MainText>
            <MainText>5:00 PM | Toasts</MainText>
            <MainText>5:15 PM | Dinner</MainText>
          </Column>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>
  </>
)

export default Timeline