import React from 'react'
import styled from 'styled-components'
import {
  Grid,
  Header
} from 'semantic-ui-react'
import { 
  BorderlessPaddedSegment,
  Divider,
  JustifiedRow,
  MainText,
  MobileAdjustedImage,
  MobileFriendlyPaddedImage,
  MobileFriendlyPaddedText,
  PaddedColumn
} from './shared-components'
import rifleFalls from '../static/images/rifle-falls.jpg'

const { Column } = Grid

// !important because semantic-ui used it
const AutoWidthMobileFriendlyPaddedText = styled(MobileFriendlyPaddedText)`
  &&&&&& {
    width: auto !important;
  }
`

const TopPaddedJustifiedRow = styled(JustifiedRow)`
  &&&& {
    padding-top: 8rem;
  }
`

const FixedWidthMainText = styled(MainText)`
  &&&& {
    width: 60rem;
    margin: auto;
    text-align: end;

    @media(max-width: 766px) {
      width: auto;
      text-align: center;
      font-size: 16px;
    }
  }
`

const Ceremony = () => (
  <>
    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow>
          <AutoWidthMobileFriendlyPaddedText floated='right' width={6}>
            <Header as='h2' style={{ fontSize: '2em' }}>Ceremony Site</Header>
            <MainText>
              <a target='_blank' rel='noreferrer' href='https://cpw.state.co.us/placestogo/parks/RifleFalls'>Rifle Falls State Park</a><br/>
              10379 CO-325<br/>
              Rifle, CO 81650
            </MainText>
          </AutoWidthMobileFriendlyPaddedText>

          <MobileFriendlyPaddedImage floated='left' width={6}>
            <MobileAdjustedImage rounded size='large' src={rifleFalls} />
          </MobileFriendlyPaddedImage>
        </JustifiedRow>
        <TopPaddedJustifiedRow>
          <PaddedColumn verticalAlign='middle'>
            <FixedWidthMainText>
              <b>
                Please be aware, there is no cell service in the mountains.<br/>
                Make sure you have downloaded directions in advance, and know where we will be, and when!
              </b>
            </FixedWidthMainText>
          </PaddedColumn>
        </TopPaddedJustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>

    <Divider />

    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow verticalAlign='middle'>
          <Column verticalAlign='middle'>
            <Header as='h2' style={{ fontSize: '2em' }}>Directions to Ceremony Site</Header>
            <MainText>
              Take I-70 to the Rifle exit (90), then go north on CO highway 13 for four miles.<br/>
              This takes you through the town of Rifle on Railroad Ave.<br/>
              Turn right onto Highway 325 and drive 9.8 miles.<br/>
              Turn right to enter Rifle Falls State Park.<br/>
              There is a public parking loop near the falls, but please consider parking in one of the sites we reserved instead.<br/>
              Parking is extremely limited and we don't want to take away from other park goers.<br/>
              We reserved campsites 4, 7, or 9 for use by our guests.
            </MainText>
            <MainText>To get to the ceremony site, please walk towards the falls and follow signs for the Mountain Mist Amphitheater.</MainText>
            <MainText>
              A map of the park can be found <a target='_blank' rel='noreferrer' href='https://cpw.state.co.us/placestogo/parks/RifleFalls/Documents/RifleFallsCampgroundMap.pdf'><b>here</b></a>.<br/>
              The amphitheater is not marked on the map, but it is near the label for the Coyote Trail.
            </MainText>
          </Column>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>

    <Divider />

    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow verticalAlign='middle'>
          <Column verticalAlign='middle'>
            <Header as='h2' style={{ fontSize: '2em' }}>Additional Information</Header>
            <MainText>There is a vehicle entry fee for the park, but we will be obtaining passes in advance and covering the entry fee for all of our guests.</MainText>
            <MainText>Following the ceremony we will be providing dinner and would love for people to hang around and spend some time enjoying the park and the company.</MainText>
            <MainText>Aside from the waterfall, there are a number of limestone caves that are great for kids to climb around in, but you may want to bring a change of clothes.</MainText>
            <MainText>We will also be bringing an RV so that we have a private space with a bathroom available to those that may need it throughout the evening for a break, somewhere to corral the kids, etc.</MainText>
          </Column>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>
  </>
)

export default Ceremony