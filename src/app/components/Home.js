import React from 'react'
import { Grid } from 'semantic-ui-react'
import { BorderlessPaddedSegment, JustifiedRow, MainText, PaddedHeader } from './shared-components'

const { Column } = Grid

const Home = () => (
  <>
    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow verticalAlign='middle'>
          <Column verticalAlign='middle'>
            <PaddedHeader as='h3'>Welcome!</PaddedHeader>
            <MainText>Thank you all for joining us in Colorado! We are honored!</MainText>
            <MainText>Here you will find more specific information about our Colorado plans. We will continue to update things here as we get more nailed down and the date gets closer.</MainText>
            <MainText>Head over to <a target='_blank' rel='noreferrer' href='https://lemmermannwedding.com'>lemmermannwedding.com</a> if you are looking for information specific to the reception, gift registry, or to view photos!</MainText>
            <MainText>Much love,<br/>Robert and Linnea</MainText>
          </Column>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>
  </>
)

export default Home