import React from 'react'
import styled from 'styled-components'
import {
  Grid,
  Header
} from 'semantic-ui-react'
import { 
  BorderlessPaddedSegment,
  Divider,
  JustifiedRow,
  MainText,
  MobileAdjustedImage,
  MobileFriendlyPaddedImage,
  MobileFriendlyPaddedText
} from './shared-components'
import rifleGap from '../static/images/rifle-gap.jpg'

const { Column } = Grid

// !important because semantic-ui used it
const AutoWidthMobileFriendlyPaddedText = styled(MobileFriendlyPaddedText)`
  &&&&&& {
    width: auto !important;
  }
`

const Dinner = () => (
  <>
    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow>
          <AutoWidthMobileFriendlyPaddedText floated='right' width={6}>
            <Header as='h2' style={{ fontSize: '2em' }}>Dinner Site</Header>
            <MainText>
              <a target='_blank' rel='noreferrer' href='https://cpw.state.co.us/placestogo/Parks/riflegap'>Rifle Gap State Park</a><br/>
              5575 CO-325<br/>
              Rifle, CO 81650
            </MainText>
          </AutoWidthMobileFriendlyPaddedText>

          <MobileFriendlyPaddedImage floated='left' width={6}>
            <MobileAdjustedImage rounded size='large' src={rifleGap} />
          </MobileFriendlyPaddedImage>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>

    <Divider />

    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow verticalAlign='middle'>
          <Column verticalAlign='middle'>
            <Header as='h2' style={{ fontSize: '2em' }}>Directions from Ceremony Site to Dinner Site</Header>
            <MainText>
              Turn left on Highway 325 and proceed for 3.8 miles.<br/>
              Turn right to enter Rifle Gap State Park.<br/>
              Follow signs for Cottonwood Campground.<br/>
              Turn left to enter the campground.<br/>
              Proceed around the loop to the parking near the group picnic area.
            </MainText>
            <MainText>A map of the park can be found <a target='_blank' rel='noreferrer' href='https://cpw.state.co.us/placestogo/parks/RifleGap/Documents/RifleGapCampgroundMap.pdf'><b>here</b></a>.</MainText>
          </Column>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>
  </>
)

export default Dinner
