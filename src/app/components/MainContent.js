import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import Activities from './Activities'
import Lodging from './Lodging'
import Ceremony from './Ceremony'
import Timeline from './Timeline'
import Dinner from './Dinner'
import Home from './Home'

const BackgroundColor = styled.div`
  background-color: #e3d9ca;
`

const content = ({ currentTab }) => {
  switch (currentTab){
    case 'lodging':
      return (<Lodging />)
    case 'ceremony':
      return (<Ceremony />)
    case 'activities':
      return (<Activities />)
    case 'timeline':
      return (<Timeline />)
    case 'dinner':
      return (<Dinner />)
    case 'home':
    default:
      return (<Home />)
  }
}

const MainContent = ({ currentTab }) => (
  <BackgroundColor>
    { content({ currentTab }) }
  </BackgroundColor>
)

const mapStateToProps = state => ({
  currentTab: state.navigation.currentTab
})

export default connect(mapStateToProps)(MainContent)
