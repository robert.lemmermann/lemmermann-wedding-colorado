import { Provider } from 'react-redux'
import MainLayout from './MainLayout'

const App = ({ store }) => (
  <Provider store={store}>
    <MainLayout />
  </Provider>
)

export default App
