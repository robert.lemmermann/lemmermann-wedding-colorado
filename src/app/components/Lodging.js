import React from 'react'
import {
  Grid
} from 'semantic-ui-react'
import styled from 'styled-components'
import holidayInn from '../static/images/holiday-inn.jpg'
import redRiverInn from '../static/images/red-river-inn.jpg'
import westKOA from '../static/images/west-koa.jpg'
import westKOAMap from '../static/images/west-koa-map.jpg'
import { 
  Divider,
  BorderlessPaddedSegment,
  JustifiedRow,
  MainText,
  MobileAdjustedImage,
  MobileFriendlyPaddedImage,
  MobileFriendlyPaddedText,
  PaddedHeader
} from './shared-components'

const { Column } = Grid

const FullWidthMobileAdjustedImage = styled(MobileAdjustedImage)`
  &&& {
    width: 100%;
  }
`

// !important used due to semantic-ui
const FullWidthMobileFriendlyPaddedImage = styled(MobileFriendlyPaddedImage)`
  &&&&& {
    width: 100% !important;
  }
`

const Lodging = () => (
  <>
    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow verticalAlign='middle'>
          <Column verticalAlign='middle'>
            <PaddedHeader as='h3'>Lodging</PaddedHeader>
            <MainText>We will be staying at the Glenwood Springs West KOA. We anticipate the KOA being a gathering point and will get as many car passes as needed for those who will be staying in lodging off site but want to spend time with us on the days surrounding the wedding.</MainText>
            <MainText>The KOA includes a pool, hot tub, splash pad, playground, dog park, Colorado River access, and lounge. These amenities are free to anyone staying on site or $2 per person per day to anyone staying elsewhere. Below you will find a map of the campground with the cabins rented by our group circled.</MainText>
            <MainText>For those who need alternative lodging, the two closest hotels are the Holiday Inn and Suites - Silt or the Red River Inn (both about a ½ mile down the road from the KOA). The Red River Inn seems to run cheaper since it does not include a pool.</MainText>
            <MainText>We included the links below, but no one is required to stay at either of them. Stay wherever you are most comfortable! There are plenty of other hotel options in Silt or the neighboring towns of Rifle and Glenwood Springs. Feel free to shop around for the best combination of price, amenities, and distance for you.</MainText>
          </Column>
        </JustifiedRow>

        <JustifiedRow>
          <FullWidthMobileFriendlyPaddedImage floated='left' width={6}>
            <FullWidthMobileAdjustedImage rounded size='large' src={westKOAMap} />
          </FullWidthMobileFriendlyPaddedImage>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>

    <Divider />

    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow>
          <MobileFriendlyPaddedText floated='right' width={6}>
            <MainText>
              <a target='_blank' rel='noreferrer' href='https://koa.com/campgrounds/colorado-river/'>Glenwood Springs West KOA</a><br/>
              629 River Frontage Rd<br/>
              Silt, CO 81652
            </MainText>
          </MobileFriendlyPaddedText>

          <MobileFriendlyPaddedImage floated='left' width={6}>
            <MobileAdjustedImage rounded size='large' src={westKOA} />
          </MobileFriendlyPaddedImage>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>

    <Divider />

    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow>
          <MobileFriendlyPaddedImage floated='right' width={6}>
            <MobileAdjustedImage rounded size='large' src={holidayInn} />
          </MobileFriendlyPaddedImage>

          <MobileFriendlyPaddedText floated='left' width={6}>
            <MainText>
              <a target='_blank' rel='noreferrer' href='https://www.ihg.com/holidayinnexpress/hotels/us/en/silt/syles/hoteldetail'>Holiday Inn and Suites</a><br/>
              1535 River Frontage Rd<br/>
              Silt, CO 81652
            </MainText>
          </MobileFriendlyPaddedText>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>

    <Divider />

    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow>
          <MobileFriendlyPaddedText floated='right' width={6}>
            <MainText>
              <a target='_blank' rel='noreferrer' href='https://www.redriverinnsiltco.com/silt-colorado-hotel-amenities.html'>Red River Inn</a><br/>
              1200 Main St<br/>
              Silt, CO 81652
            </MainText>
          </MobileFriendlyPaddedText>

          <MobileFriendlyPaddedImage floated='left' width={6}>
            <MobileAdjustedImage rounded size='large' src={redRiverInn} />
          </MobileFriendlyPaddedImage>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>
  </>
)

export default Lodging