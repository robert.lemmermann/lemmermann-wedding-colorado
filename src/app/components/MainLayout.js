import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
  Container,
  Image,
  Menu,
  Segment
} from 'semantic-ui-react'
import engagementImage from '../static/images/engagement-monogram.jpg'
import { 
  lodgingClicked,
  activitiesClicked,
  ceremonyClicked,
  homeClicked,
  timelineClicked,
  dinnerClicked
} from '../store/actions'
import MainContent from './MainContent'
import { Divider } from './shared-components'

const SolidBackgroundTabularMenu = styled(Menu)`
  &&& {
    background: #64604b;
    padding-top: 0;
    height: auto;

    @media(min-width: 767px) {
      height: 4.6rem;
      padding-top: 0.5rem;

      div {
        position: absolute;
        bottom: 0;
        width: 100%;
        padding-left: 4rem;
        height: 3.6rem;
      }
    }
  }
`

const MenuLink = styled.a`
  &&&&{
    border-left: 1px solid rgba(255, 255, 255, 0.5);
    border-right: 1px solid rgba(255, 255, 255, 0.5);
    border-top: 1px solid rgba(255, 255, 255, 0.5);
    border-bottom: none;

    :last-of-type {
      @media(max-width: 766px) {
        border-bottom: 1px solid rgba(255, 255, 255, 0.5);
      }
    }
  }
`

const NoPaddedSegment = styled(Segment)`
  &&& {
    padding: 0;
  }
`

const ImageContainerSegment = styled(NoPaddedSegment)`
  &&& {
    position: relative;
    width: 100%;
    height: calc(100% - 15% - 3.6rem);
    max-height: calc(100% - 15% - 3.6rem);
    overflow: hidden;
    border: none;

    @media(max-width: 766px) {
      height: 100vw;
    }
  }
`

const MenuContainer = styled(Container)`
  &&& {
    @media(max-width: 766px) {
      a {
        height: 2rem;
      }
    }
  }
`

const MobileAdjustedSegment = styled(NoPaddedSegment)`
  &&& {
    @media(max-width: 766px) {
      height: 6rem;
    }
  }
`

const FormattedImage = styled(Image)`
  &&& {
    position: absolute;
    max-width: 100%;
    width: 100%;
    height: auto;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    @media(max-width: 766px){
      width: auto;
      height: 100vw;
    }
  }
`

const Footer = styled.div`
  background-color: #64604b;
  height: 4.6rem;
`

const MainLayout = ({ currentTab, lodgingClickedAction, ceremonyClickedAction, activitiesClickedAction, homeClickedAction, timelineClickedAction, dinnerClickedAction }) => (
  <>
    <MobileAdjustedSegment attached>
      <SolidBackgroundTabularMenu
        fixed='top'
        tabular
        inverted
        stackable
      >
        <MenuContainer>
          <SolidBackgroundTabularMenu.Item as={MenuLink} active={currentTab === 'home'} name='home' onClick={homeClickedAction}></SolidBackgroundTabularMenu.Item>
          <SolidBackgroundTabularMenu.Item as={MenuLink} active={currentTab === 'timeline'} name='timeline' onClick={timelineClickedAction}></SolidBackgroundTabularMenu.Item>
          <SolidBackgroundTabularMenu.Item as={MenuLink} active={currentTab === 'ceremony'} name='ceremony' onClick={ceremonyClickedAction}></SolidBackgroundTabularMenu.Item>
          <SolidBackgroundTabularMenu.Item as={MenuLink} active={currentTab === 'dinner'} name='dinner' onClick={dinnerClickedAction}></SolidBackgroundTabularMenu.Item>
          <SolidBackgroundTabularMenu.Item as={MenuLink} active={currentTab === 'lodging'} name='lodging' onClick={lodgingClickedAction}></SolidBackgroundTabularMenu.Item>
          <SolidBackgroundTabularMenu.Item as={MenuLink} active={currentTab === 'activities'} name='other activities' onClick={activitiesClickedAction}></SolidBackgroundTabularMenu.Item>
        </MenuContainer>
      </SolidBackgroundTabularMenu>
    </MobileAdjustedSegment>

    <ImageContainerSegment
      attached='top'
    >
      <FormattedImage src={engagementImage} fluid/>
    </ImageContainerSegment>

    <Divider />

    <MainContent />

    <Footer />
  </>
)

const mapStateToProps = state => ({
  currentTab: state.navigation.currentTab
})

const mapDispatchToProps = dispatch => ({
  lodgingClickedAction: () => dispatch(lodgingClicked()),
  activitiesClickedAction: () => dispatch(activitiesClicked()),
  ceremonyClickedAction: () => dispatch(ceremonyClicked()),
  homeClickedAction: () => dispatch(homeClicked()),
  timelineClickedAction: () => dispatch(timelineClicked()),
  dinnerClickedAction: () => dispatch(dinnerClicked())
})

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout)
