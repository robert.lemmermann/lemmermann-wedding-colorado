import styled from 'styled-components'
import {
  Grid,
  Header,
  Image,
  Segment
} from 'semantic-ui-react'

const { Column, Row } = Grid

export const Divider = styled.div`
  background-color: #9ba07c;
  height: 3.6rem;
`

export const BorderlessPaddedSegment = styled(Segment)`
  &&& {
    padding: 6rem 2rem 8rem;
    border: none;

    @media(min-width: 767px) {
      padding: 6rem 0 8rem;
    }
  }
`

export const MainText = styled.p`
  padding-bottom: 1rem;
  font-size: 1.33rem;
`

export const SubText = styled(MainText)`
  padding-left: 2rem;
  color: grey;
`

export const JustifiedRow = styled(Row)`
  &&&& {
    justify-content: center;
  }
`

export const PaddedColumn = styled(Column)`
  &&&& {
    margin-left: auto;
    margin-right: auto;
  }
`

export const PaddedHeader = styled(Header)`
  padding-bottom: 2rem;
  font-size: 2rem;
`

export const PaddedRow = styled(Row)`
  &&& {
    padding-bottom: 5rem;
  }
`

export const JustifiedPaddedRow = styled(PaddedRow)`
  &&&& {
    justify-content: center;
  }
`

export const MobileFriendlyPaddedImage = styled(PaddedColumn)`
  &&&&&& {
    display: flex;
    align-items: center;
  }
`

//!important was required because semantic ui utilized it for this styling it's overriding
export const MobileFriendlyPaddedText = styled(PaddedColumn)`
  &&&&&& {
    @media(max-width: 766px) {
      width: auto !important;
    }
  }
`

export const MobileAdjustedImage = styled(Image)`
  &&& {
    @media(max-width: 766px) {
      width: 300px;
    }
  }
`
