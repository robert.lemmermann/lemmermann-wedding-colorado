import React from 'react'
import { Grid } from 'semantic-ui-react'
import { BorderlessPaddedSegment, JustifiedRow, MainText, PaddedHeader } from './shared-components'

const { Column } = Grid

const Activities = () => (
  <>
    <BorderlessPaddedSegment vertical>
      <Grid container stackable verticalAlign='middle'>
        <JustifiedRow verticalAlign='middle'>
          <Column verticalAlign='middle'>
            <PaddedHeader as='h3'>Other Activities</PaddedHeader>
            <MainText>For those spending more time in the area than for just the ceremony and/or who want to do more than just see us, here's some websites with local activities:</MainText>
            <MainText><a target='_blank' rel='noreferrer' href='https://visitglenwood.com/things-to-do/'>Visit Glenwood</a></MainText>
            <MainText><a target='_blank' rel='noreferrer' href='https://visitrifle.com/'>Visit Rifle</a></MainText>
            <MainText><a target='_blank' rel='noreferrer' href='https://www.visitgrandjunction.com/'>Visit Grand Junction</a></MainText>
          </Column>
        </JustifiedRow>
      </Grid>
    </BorderlessPaddedSegment>
  </>
)

export default Activities